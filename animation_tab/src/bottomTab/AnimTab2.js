import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React, { useEffect, useRef } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Icon, { Icons } from '../components/Icons';
import Colors from '../constants/Colors';
import ColorScreen from '../screens/ColorScreen';
import * as Animatable from 'react-native-animatable';

const TabArr = [
  { route: 'contacts', label: 'Home', type: Icons.Feather, icon: 'search', component: ColorScreen },
  { route: 'Search', label: 'Search', type: Icons.Feather, icon: 'credit-card', component: ColorScreen },
  { route: 'Add', label: 'Add', type: Icons.Feather, icon: 'briefcase', component: ColorScreen },
  { route: 'Like', label: 'Like', type: Icons.Feather, icon: 'calendar', component: ColorScreen },
  { route: 'Account', label: 'Account', type: Icons.FontAwesome, icon: 'user-circle-o', component: ColorScreen },
];

const Tab = createBottomTabNavigator();

const animate1 = { 0: { scale: .5, translateY: 0 }, .1: { translateY: 1 }, 1: { scale: 1.2, translateY: -10 } }
const animate2 = { 0: { scale: 1.2, translateY: -24 }, 1: { scale: 1, translateY: 7 } }

const circle1 = { 0: { scale: 5 }, 0.3: { scale: .9 }, 0.5: { scale: .2 }, 0.8: { scale: .7 }, 1: { scale: 1 } }
const circle2 = { 0: { scale: 1 }, 1: { scale: 0 } }

const TabButton = (props) => {
  const { item, onPress, accessibilityState } = props;
  const focused = accessibilityState.selected;
  const viewRef = useRef(null);
  const circleRef = useRef(null);
  const textRef = useRef(null);

  useEffect(() => {
    if (focused) {
      viewRef.current.animate(animate1);
      circleRef.current.animate(circle1);
      textRef.current.transitionTo({ scale: 1 });
    } else {
      viewRef.current.animate(animate2);
      circleRef.current.animate(circle2);
      textRef.current.transitionTo({ scale: 0 });
    }
  }, [focused])

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={styles.container}>
      <Animatable.View
        ref={viewRef}
        duration={1000}
        style={styles.container}>
        <View style={styles.btn}>
          <Animatable.View
            ref={circleRef}
            style={styles.circle} />
          <Icon type={item.type} name={item.icon} color={focused ? Colors.primary : Colors.primary} size={22}/>
        </View>

        <Animatable.Text
          ref={textRef}
          style={styles.text}>
          {item.label}
        </Animatable.Text>

      </Animatable.View>
    </TouchableOpacity>
  )
}

export default function AnimTab1() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: styles.tabBar,
      }}
    >
      {TabArr.map((item, index) => {
        return (
          <Tab.Screen key={index} name={item.route} component={item.component}
            options={{
              tabBarShowLabel: false,
              tabBarButton: (props) => <TabButton {...props} item={item} />
            }}
          />
        )
      })}
    </Tab.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: Colors.yellow,
    marginLeft:15,
  },
  tabBar: {
    height: 70,
    position: 'absolute',
    borderColor:'#fff'
    
  },
  btn: {
    width: 50,
    height: 50,
    borderRadius: 30,
    
    backgroundColor: Colors.white,
    alignItems:'center',
    justifyContent:'center',
  },
  circle: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'lightskyblue',
    borderRadius: 50,
    width: 37,
    height: 37,
    marginTop:7,
    marginLeft: 7,
    alignItems:'center',
    justifyContent:'center',
  },
  text: {
    fontSize: 10,
    textAlign: 'center',
    marginLeft: 15,
    color: Colors.primary,
  }
})