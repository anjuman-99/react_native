import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View ,Image} from 'react-native'
import * as Animatable from 'react-native-animatable'
import Styles from '../common/Styles';
import Colors from '../constants/Colors';
 
import Ionicons from '@expo/vector-icons/Ionicons';

export default function ColorScreen({ route, navigation }) {
  const viewRef = React.useRef(null);
  const [bgColor, setBgColor] = useState();
  useEffect(() => {
    switch (route.name) {
      case 'Home': { setBgColor(Colors.bg); break; }
      case 'Search': { setBgColor(Colors.bg); break; }
      case 'Add': { setBgColor(Colors.bg); break; }
      case 'Account': { setBgColor(Colors.bg); break; }
      case 'Like': { setBgColor(Colors.bg); break; }
      default: setBgColor(Colors.bg);
    }
  }, [])
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      viewRef.current.animate({ 0: { opacity: 0.5, }, 1: { opacity: 1 } });
    })
    return () => unsubscribe;
  }, [navigation])
  return (
    <View style={Styles.container}>
      

      <View style={{alignItems:'center', justifyContent:'center',}}>  
          <View style={{
         height: 55, width: '92%',paddingTop:10,  flexDirection: 'row', justifyContent: 'space-between', 
      }}>
        <Ionicons name="arrow-back-outline" size={30} color="#000000" />
        <View style={{height:40, width:40, backgroundColor:'#FFFFFF', borderRadius:50, alignItems:'center', justifyContent:'center',}}>
          <Ionicons name="notifications-outline" size={30} color="#115067" /></View>
       
        </View>

        {/* img */}
        <View style={{
          width: '100%', 
      }}>
      <Image source={require('../img/Group1.png')}
       style={{width: "100%", height: 140}} />
      </View> 
        {/*  */}

        <View style={{
        width: '90%', 
              }}>
        <Text style={{
        color: '#115067',
        fontSize:15,
      }}>Select the shift and staffs requirement</Text>
      </View>


{/*  */}
<View style={{
        width: '92%', height: 320, backgroundColor: '#fff',
        borderRadius : 15,
        alignItems:'center',
        justifyContent:"center",
        marginTop:10,
              }}>
        <View style={{
          width: '90%', 
          height: 320,
          backgroundColor: '#fff',
          
          }}>

          <Text style={{
            color:'#4D4D4D',
            marginTop:10,
            marginBottom:10,
          }}>Shift Timing </Text>

          <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom:10,}}>
          <View style={{backgroundColor:'#ECEEF0',width:135, height:34, borderRadius : 8, justifyContent:'center', paddingLeft:15,}}>
            <Text style={{color:'#115067'}}>12:30</Text>
          </View>
          <View style={{backgroundColor:'#ECEEF0',width:135, height:34, borderRadius : 8, justifyContent:'center', paddingLeft:15, }}>
          <Text style={{color:'#115067'}}>06:00</Text>
          </View>
          </View>

          <Text style={{color:'#4D4D4D', marginTop:10,}}>Working Days</Text>

          <View style={{flexDirection: 'row', justifyContent: 'space-between',  marginTop:15,}}>
                <View style={{backgroundColor:'#0089D0',width:35, height:34, borderRadius : 8, alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#FFFF',fontWeight: 'bold',}}>M</Text>
                </View>
                <View style={{backgroundColor:'#0089D0',width:35, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#FFFF',fontWeight: 'bold',}}>T</Text>
                </View>
                <View style={{backgroundColor:'#0089D0',width:35, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#FFFF',fontWeight: 'bold',}}>W</Text>
                </View>
                <View style={{backgroundColor:'#0089D0',width:35, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#FFFF',fontWeight: 'bold',}}>T</Text>
                </View>
                <View style={{backgroundColor:'#0089D0',width:35, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#FFFF',fontWeight: 'bold',}}>F</Text>
                </View>
                <View style={{backgroundColor:'#0089D0',width:35, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#FFFF',fontWeight: 'bold',}}>S</Text>
                </View>
                <View style={{backgroundColor:'#ECEEF0',width:35, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#4D4D4D',fontWeight: 'bold',}}>S</Text>
                </View>
          </View>
          <Text style={{ marginTop:10, color:'#4D4D4D',}}>Approximate Daily Wage</Text>
        
          <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop:10,}}>
                <View style={{borderColor:'#858B8F',borderWidth:1, padding:2, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#858B8F',}}>Up to 500</Text>
                </View>

                <View style={{borderColor:'#858B8F',borderWidth:1, padding:2, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#858B8F',}}>500-700</Text>
                </View>

                <View style={{borderColor:'#858B8F',borderWidth:1, padding:2, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#858B8F',}}>700-1000</Text>
                </View>

                <View style={{borderColor:'#858B8F',borderWidth:1, padding:2, height:34, borderRadius : 8,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#858B8F',}}>1000 or more</Text>
                </View>
            </View>
          
            <Text style={{ marginTop:10, color:'#4D4D4D',}}>Experience Level</Text>


            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop:15,}}>
                <View style={{borderColor:'#858B8F',borderWidth:1, width:70, height:34, borderRadius : 4,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#858B8F',}}>0-2 yrs</Text>
                </View>

                <View style={{borderColor:'#858B8F',borderWidth:1, width:70, height:34, borderRadius : 4,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#858B8F',}}>2-5 yrs</Text>
                </View>

                <View style={{borderColor:'#858B8F',borderWidth:1, width:60, height:34, borderRadius : 4,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#858B8F',}}>5-10 yrs</Text>
                </View>

                <View style={{borderColor:'#858B8F',borderWidth:1,  width:60, height:34, borderRadius : 4,alignItems:'center', justifyContent:'center'}}>
                <Text style={{color:'#858B8F', }}>Expert</Text>
                </View>
            </View>
        </View>

    </View>

    {/*  */}


    <View style={{
        width: 150, height: 45, backgroundColor: '#0089D0',borderRadius : 8, alignItems:"center",marginTop:15, marginBottom:25,
      }}>
        <Text style={{ 
          color: '#fff',
          marginTop:10,
          fontWeight: 'bold',
          fontSize: 15,}}>Proceed</Text>
      </View>
     

      </View> 
      <Animatable.View
        ref={viewRef}
        easing={'ease-in-out'}
        style={Styles.container}>
        <View style={{backgroundColor: bgColor, flex: 1}} />
      </Animatable.View>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    alignItems:'center',
     justifyContent:'center',
  }
})
