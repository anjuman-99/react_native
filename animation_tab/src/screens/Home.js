import React from 'react'
import { List } from 'react-native-paper';
import { StyleSheet, View } from 'react-native'

const Home = ({ navigation }) => {

  const navigate = (route) => navigation.navigate(route)
  return (
    <View styles={{ flex: 1 }}>
    <List.Item title=" Bottom Navigation Animatable" onPress={() => navigate('Tab2')} />
    </View>
  )
}

export default Home

const styles = StyleSheet.create({})
