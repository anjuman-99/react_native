import React, { useState } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar } from 'react-native';
import Card from './src/Card';
import  Header  from './src/Header';


export default function App(){
 
  const [data, setData] = useState([
    {
      id: 1,
      name: 'Zoya Ansari',
      email:'zoya@gmail.com',
      age:9,
    },
    {
      id: 2,
      name: 'Ruhi Ansari',
      email:'ruhi@gmail.com',
      age:5,
    },
    {
      id: 3,
      name: 'duggu pagl',
      email:'paglduggu@gmail.com',
      age:9,
    },
  ]) 

  return (
    <View>
    <Header />
    <View>
    <FlatList
        data={data}
        keyExtractor={item => item.id}
        renderItem={ ({item})=> <Card {...item}/> }
      />
    </View>
    </View>
    
  );
} 



const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  name: {
    color:'red',
    fontSize:20,
  },
  title: {
    fontSize: 32,
  },
});



