import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

function Card({
    name,
    email
}) {

  return (
    <View style={styles.container}>
        <Text style={styles.text}>
            {name}
        </Text>

        <Text style={styles.text}>
        {email}
        </Text>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
     height:100,
     backgroundColor:'#cecece',
     alignContent:'center'
    },
    text:{
      marginTop:10,
    }
    
  });

export default Card