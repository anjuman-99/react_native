import React from 'react';
import {  View, } from 'react-native';
import TodoApp from './Components/App';


export default function App() {
    
  return (
    <View>
     <TodoApp />
    </View>
  );
}

